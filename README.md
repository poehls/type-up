# type-up
type-up is just another Speed Typing tool - but a very kind one :sparkling_heart:

Train your typing skills and lift your mood at the same time by typing reassuring positive self-talk messages. type-up empowers you to be your own motivational coach :sparkles:

## Project status
This is a hobby project at a very early stage of development with no clear timeline. If you happen to be interested in using the app please be patient :pray: or reach out to contribute :muscle:

## License
type-up is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

type-up is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with type-up. If not, see <https://www.gnu.org/licenses/>.
